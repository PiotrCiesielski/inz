var should = require('should'),
    Mod    = require('../../src/js/salbp1');

let problem = () => {
  return {
    operations: [
      { id: 1, time: 2 },
      { id: 2, time: 1, preds: [1] },
      { id: 3, time: 4, preds: [2] },
      { id: 4, time: 3, preds: [1] },
      { id: 5, time: 2, preds: [3, 4] }
    ],

    cycle: 5
  };
};

let emptySolution = () => {
  return {
    weights: [],
    stations: [],
    indices: {}
  };
};

let rpwWeights = () => {
  return [
    { id: 1, weight: 9 },
    { id: 2, weight: 7 },
    { id: 3, weight: 6 },
    { id: 4, weight: 5 },
    { id: 5, weight: 2 }
  ];
};

describe('Salbp1', () => {

  var prob, sol;

  beforeEach( () => {
    prob = problem();
  });

  it("First decision - RPW weights", () => {
    let decisions = Mod.decisions(prob, emptySolution());

    decisions.should.be.an.Array();
    decisions = decisions.filter( (d) => d.method === 'rpw' );

    decisions.should.be.deepEqual([
      { type: 'weights', method: 'rpw',
        values: rpwWeights() }
    ]);

    // the problem should remain unchanged!
    prob.should.be.deepEqual( problem() );
  });

  let deepCheck = (decisions, obj) => {
    decisions.should.be.an.Array();
    decisions.should.be.deepEqual([ obj ]);
  };

  let containCheck = (decisions, arr) => {
    decisions.should.be.an.Array();
    decisions.should.containDeep( arr );
  };

  it("RPW, 1st operation", () => {
    sol = emptySolution();
    sol.weights = rpwWeights();
    sol.stations = [ [] ];

    let decisions = Mod.decisions(prob, sol);

    deepCheck(decisions, { type: 'operation', id: 1 } );

    // the problem should remain unchanged!
    prob.should.be.deepEqual( problem() );
  });

  it("RPW, 2nd operation", () => {
    sol = emptySolution();
    sol.weights = rpwWeights();
    sol.stations = [ 
      [ { id: 1, start: 0, end: 2 } ] 
    ];

    let decisions = Mod.decisions(prob, sol);

    deepCheck(decisions, { type: 'operation', id: 2 } );

    // the problem should remain unchanged!
    prob.should.be.deepEqual( problem() );
  });

  // dalej analogicznie dopisać testy
  // kolejne decyzje:
  //   { type: 'new-station' }
  //   { type: 'operation', id: 3 }
  //   { type: 'new-station' }
  //   { type: 'operation', id: 4 }
  //   { type: 'operation', id: 5 }
  // 
  // i teraz się zacznie:

  it("RPW, indices", () => {
    sol = emptySolution();
    sol.weights = rpwWeights();
    sol.stations = [ 
      [ { id: 1, start: 0, end: 2 },
        { id: 2, start: 2, end: 3 } ],
      [ { id: 3, start: 0, end: 4 } ],
      [ { id: 4, start: 0, end: 3 },
        { id: 5, start: 3, end: 5 } ]
    ];

    let decisions = Mod.decisions(prob, sol);

    containCheck(decisions, [
      { type: 'le', value: 80 },
      { type: 'si', value: 2.24 },
      { type: 'lt', value: 15 }
    ]);

    // the problem should remain unchanged!
    prob.should.be.deepEqual( problem() );
  });

  // when the weights are equal...  
  it("The same weights, 2nd operation", () => {
    sol = emptySolution();
    sol.weights = rpwWeights();
    sol.weights.filter( (w) => w.id === 4 )[0].weight = 7;
    sol.stations = [ 
      [ { id: 1, start: 0, end: 2 } ] 
    ];

    let decisions = Mod.decisions(prob, sol);

    containCheck(decisions, [
      { type: 'operation', id: 2 },
      { type: 'operation', id: 4 }
    ]);

    // the problem should remain unchanged!
    prob.should.be.deepEqual( problem() );
  });

  // and when the station time is not long enough
  it("Operations allowed due to station time", () => {
    sol = emptySolution();
    sol.weights = rpwWeights();
    sol.weights.filter( (w) => w.id === 4 )[0].weight = 7;
    sol.stations = [ 
      [ { id: 1, start: 0, end: 2 } ] 
    ];
    prob.operations.filter( (o) => o.id === 4 )[0].time = 4;

    let decisions = Mod.decisions(prob, sol);

    containCheck(decisions, [
      { type: 'operation', id: 2 }
    ]);

    // the problem should remain unchanged!
    prob.should.be.deepEqual( problem() );
  });

});
