// ------------------------------------------------------------------
// Private functions

// Are the weights known?
// -> true/false
var weightsKnown = (solution) => {
  return (! solution.hasOwnProperty('weights')) || (solution.weights.length === 0);
};

// Returns the ids of all assigned operations.
var assignedOperations = (solution) => {
  return solution.stations.reduce( (acc, st) => {
    let ids = st.map( (op) => op.id );
    return acc.concat(ids);
  }, [] );
};

// Not a perfect implementaion...
var allOperationsAssigned = (problem, solution) => {
  let assigned_ops = assignedOperations(solution),
      all_ops = problem.operations.map( (op) => op.id );

  return assigned_ops.length === all_ops.length;
};

var rpwWeights = (problem) => {
  // todo...
};

var weightsDec = (method, values) => {
  return {
    type: 'weights',
    method, values
  };
};

// prepare the weights decisions
var weightsDecisions = (problem) => {
  let rpw = rpwWeights(problem);

  return [
    weightsDec('rpw', rpw)
  ];
};

// ------------------------------------------------------------------
// Public functions

var decisions = function( problem, solution ) {
  // when weights are not known, it's the beginning
  if ( ! weightsKnown(solution) )
    return weightsDecisions(problem);

  // when the full solution is known, it's time to calculate the indices
  if ( allOperationsAssigned(problem, solution) )
    return indicesDecisions(solution);

  // when the solution is complete, nothing more can be done
  if ( solutionComplete(problem, solution) )
    return noDecisions();

  // otherwise, calculate the next decisions
  if ( moreOperationsPossible(problem, solution) )
    return operationsDecisions(problem, solution);
  else
    return newStationDecision();
};

exports.decisions = decisions;
